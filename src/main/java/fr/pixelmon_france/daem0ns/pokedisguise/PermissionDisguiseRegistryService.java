package fr.pixelmon_france.daem0ns.pokedisguise;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PermissionDisguiseRegistryService
implements DisguiseRegistryService {
    @Override
    public boolean hasDisguise(EntityPlayerMP player, String disguise) {
        return player.canUseCommand(0, this.getPermission(disguise.split(",")[0])) || player.canUseCommand(0, this.getOtherPermission(disguise.split(",")[0]));
    }

    @Override
    public void save(EntityPlayerMP player) {
    }

    @Override
    public void giveDisguise(EntityPlayerMP player, String disguise) {
    	FMLCommonHandler.instance().getMinecraftServerInstance().commandManager.executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "lp user " + player.getName() + " permission set " + this.getPermission(disguise.split(",")[0]) + " true");
    }

    @Override
    public void removeDisguise(EntityPlayerMP player, String disguise) {
    	FMLCommonHandler.instance().getMinecraftServerInstance().commandManager.executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "lp user " + player.getName() + " permission unset " + this.getPermission(disguise.split(",")[0]));
    }

    private String getPermission(String from) {
        return "pokedisguise." + from.replace(" ", "_");
    }

    private String getOtherPermission(String from) {
        return "pd." + from.replace(" ", "_");
    }
}

