package fr.pixelmon_france.daem0ns.pokedisguise;

import net.minecraft.entity.player.EntityPlayerMP;

public interface PlayerHideService {
    void hide(EntityPlayerMP var1);

    void show(EntityPlayerMP var1);
}

