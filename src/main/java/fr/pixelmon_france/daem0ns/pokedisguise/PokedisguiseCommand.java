package fr.pixelmon_france.daem0ns.pokedisguise;

import com.google.common.collect.Lists;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.util.*;

public class PokedisguiseCommand extends CommandBase {

    public String getName() {
        return "pokedisguise";
    }

    @Override
    public List<String> getAliases() {
        return Lists.newArrayList("pdisguise", "disguise", "pd");
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/disguise <player> <specs>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(new TextComponentString(TextFormatting.AQUA + "PokéDisguise by " + TextFormatting.AQUA + "Justin for sponge !"));
            sender.sendMessage(new TextComponentString(TextFormatting.AQUA + "Ported by " + TextFormatting.RED + "DaeM0nS " + TextFormatting.AQUA + "to forge only!"));
            sender.sendMessage(new TextComponentString(TextFormatting.RED + getUsage(sender)));
        } else {
            if (!sender.canUseCommand(0, "pokedisguise.disguise")) {
                sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have the permission to execute this command!"));
                return;
            }
            if (!sender.canUseCommand(0, "pokedisguise.admin")) {
                if (sender instanceof EntityPlayerMP) {
                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("off")) {
                            Optional.ofNullable(PokeDisguise.getDisguises().remove((EntityPlayerMP) sender)).ifPresent(net.minecraft.entity.Entity::setDead);
                            PokeDisguise.getInstance().vphs.show((EntityPlayerMP) sender);
                            return;
                        }
                        String s = args[0].split(",")[0];

                        if (!EnumSpecies.hasPokemon(s)) {
                            sender.sendMessage(new TextComponentString(TextFormatting.RED + "This pokemon (" + s + ") doesn't exists."));
                            return;
                        }

                        PokemonSpec spec = new PokemonSpec(args[0].split(","));
                        if (PokeDisguise.getService().hasDisguise((EntityPlayerMP) sender, spec.name) || sender.canUseCommand(0, "pokedisguise.all")) {
                            PokeDisguise.disguise((EntityPlayerMP) sender, spec);
                            return;
                        }
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't own this disguise!"));

                    } else {
                        if (args.length == 2 && args[1].contains(",")) {
                            String[] split = args[1].split(",");
                            String[] specs = Arrays.copyOfRange(split, 1, split.length);
                            PokemonSpec spec = new PokemonSpec(specs);
                            if (!EnumSpecies.hasPokemon(spec.name)) {
                                sender.sendMessage(new TextComponentString(TextFormatting.RED + "This pokemon (" + spec.name + ") doesn't exists."));
                                return;
                            }
                            PokeDisguise.disguise((EntityPlayerMP) sender, spec);
                            return;
                        }
                        String[] specs;
                        if (Arrays.asList(server.getPlayerList().getOnlinePlayerNames()).contains(args[0])) {
                            if (!sender.canUseCommand(0, "pokedisguise.other")) {
                                sender.sendMessage(new TextComponentString(TextFormatting.RED + "You can't disguise someone else!"));
                                return;
                            }
                            specs = Arrays.copyOfRange(args, 1, args.length);
                        } else {
                            specs = Arrays.copyOfRange(args, 0, args.length);
                        }
                        PokemonSpec spec = new PokemonSpec(specs);
                        if (!EnumSpecies.hasPokemon(spec.name)) {
                            sender.sendMessage(new TextComponentString(TextFormatting.RED + "This pokemon (" + spec.name + ") doesn't exists."));
                            return;
                        }
                        if (Arrays.asList(server.getPlayerList().getOnlinePlayerNames()).contains(args[0])) {
                            PokeDisguise.disguise(server.getPlayerList().getPlayerByUsername(args[0]), spec);
                        } else {
                            PokeDisguise.disguise((EntityPlayerMP) sender, spec);
                        }
                    }
                } else {
                    sender.sendMessage(new TextComponentString(TextFormatting.RED + "You need to be a player to execute this command!"));
                }
            } else {
                if (args[0].equalsIgnoreCase("give")) {
                    if (!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[1])) {
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "This player isn't connected!"));
                        return;
                    }
                    EntityPlayerMP player2 = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[1]);
                    if (PokeDisguise.getService().hasDisguise(player2, args[2])) {
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "The player already have this disguise"));
                        return;
                    }
                    PokeDisguise.getService().giveDisguise(player2, args[2]);
                    sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been given"));
                } else if (args[0].equalsIgnoreCase("remove")) {
                    if (!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[1])) {
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "This player isn't connected!"));
                        return;
                    }
                    EntityPlayerMP player2 = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[1]);
                    if (PokeDisguise.getService().hasDisguise(player2, args[2])) {
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "The player doesn't have this disguise"));
                        return;
                    }
                    PokeDisguise.getService().removeDisguise(player2, args[2]);
                    sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been removed"));
                } else if (args[0].equalsIgnoreCase("off")) {
                    if(args.length==1){
                        Optional.ofNullable(PokeDisguise.getDisguises().remove((EntityPlayerMP)sender)).ifPresent(p -> {
                            p.setDead();
                            PokeDisguise.getInstance().vphs.show((EntityPlayerMP) sender);
                            sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been disabled"));
                        });
                    }else {
                        if (!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[1])) {
                            sender.sendMessage(new TextComponentString(TextFormatting.RED + "This player isn't connected!"));
                            return;
                        }
                        EntityPlayerMP player2 = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[1]);
                        Optional.ofNullable(PokeDisguise.getDisguises().remove(player2)).ifPresent(p -> {
                            p.setDead();
                            PokeDisguise.getInstance().vphs.show(player2);
                            sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been disabled"));
                        });
                    }
                } else {
                    String[] specs;
                    if (Arrays.asList(server.getPlayerList().getOnlinePlayerNames()).contains(args[0])) {
                        specs = Arrays.copyOfRange(args, 1, args.length);
                    } else {
                        specs = Arrays.copyOfRange(args, 0, args.length);
                    }
                    PokemonSpec spec = new PokemonSpec(specs);
                    if (!EnumSpecies.hasPokemon(spec.name)) {
                        sender.sendMessage(new TextComponentString(TextFormatting.RED + "This pokemon (" + spec.name + ") doesn't exists."));
                        return;
                    }
                    if (Arrays.asList(server.getPlayerList().getOnlinePlayerNames()).contains(args[0])) {
                        PokeDisguise.disguise(server.getPlayerList().getPlayerByUsername(args[0]), spec);
                    } else {
                        PokeDisguise.disguise((EntityPlayerMP) sender, spec);
                    }
                }
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        ArrayList<String> s = new ArrayList<>();
        s.add("off");
        s.addAll(EnumSpecies.getNameList());

        if (sender.canUseCommand(0, "pokedisguise.admin") || sender.canUseCommand(0, "pokedisguise.other")) {
            if (args.length == 1) {
                ArrayList<String> cpl = new ArrayList<>();
                cpl.addAll(Arrays.asList("give", "off", "remove"));
                cpl.addAll(Arrays.asList(server.getOnlinePlayerNames()));
                return CommandBase.getListOfStringsMatchingLastWord(args, cpl);
            } else if (args.length == 2) {
                if (Arrays.asList("give", "remove", "off").contains(args[0])) {
                    return CommandBase.getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
                } else if (Arrays.asList(server.getOnlinePlayerNames()).contains(args[0])) {
                    return CommandBase.getListOfStringsMatchingLastWord(args, s);
                }
            }
            return CommandBase.getListOfStringsMatchingLastWord(args, s);
        } else {
            if (args.length == 1) {
                return CommandBase.getListOfStringsMatchingLastWord(args, s);
            } else {
                return Collections.emptyList();
            }
        }
    }
}
