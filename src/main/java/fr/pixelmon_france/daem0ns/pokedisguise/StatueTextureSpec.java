package fr.pixelmon_france.daem0ns.pokedisguise;

import com.pixelmonmod.pixelmon.api.pokemon.ISpecType;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.SpecValue;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumStatueTextureType;
import net.minecraft.nbt.NBTTagCompound;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class StatueTextureSpec extends SpecValue<String> implements ISpecType {
    private String value;
    private List<String> keys;

    public StatueTextureSpec(List<String> keys, String value) {
        super(keys.get(0), value);
        this.keys = keys;
        this.value = value;
    }

    public List<String> getKeys() {
        return this.keys;
    }

    public Class getSpecClass() {
        return this.getClass();
    }

    public Class getValueClass() {
        return String.class;
    }

    public StatueTextureSpec parse(String arg) {
        return new StatueTextureSpec(this.keys, arg);
    }

    public String toParameterForm(SpecValue value) {
        return value.key + ":" + value.value.toString();
    }

    public SpecValue<String> clone() {
        return new StatueTextureSpec(this.keys, this.value);
    }

    public StatueTextureSpec readFromNBT(NBTTagCompound nbt) {
        return this.parse(String.valueOf(nbt.getByte("statueTexture")));
    }

    public void writeToNBT(NBTTagCompound nbt, SpecValue value) {
        if (this.value.length() < 3 && StringUtils.isNumeric(this.value)) {
            nbt.setByte("statueTexture", (byte) EnumStatueTextureType.getFromOrdinal(Integer.parseInt(this.value)).index);
        } else {
            nbt.setByte("statueTexture", (byte) EnumStatueTextureType.getFromString(this.value).index);
        }
    }

    public boolean matches(EntityPixelmon pixelmon) {
        return this.matches(pixelmon.getPokemonData());
    }

    public boolean matches(Pokemon pokemon) {
        return pokemon.getPersistentData().hasKey("statueTexture") && pokemon.getPersistentData().getString("statueTexture").equalsIgnoreCase(this.value);
    }

    public boolean matches(NBTTagCompound nbt) {
        if (!nbt.hasKey("statueTexture")) {
            return false;
        }
        if (this.value.length() < 3 && StringUtils.isNumeric(this.value)) {
            return nbt.getByte("statueTexture") == (byte) EnumStatueTextureType.getFromOrdinal(Integer.parseInt(this.value)).index;
        } else {
            return nbt.getByte("statueTexture") == (byte) EnumStatueTextureType.getFromString(this.value).index;
        }
//        return nbt.getString("statueTexture").equals(this.value);
    }

    public void apply(EntityPixelmon pixelmon) {
        this.apply(pixelmon.getPokemonData());
    }

    public void apply(Pokemon pokemon) {
        pokemon.getPersistentData().setString("statueTexture", this.value);
    }

    public void apply(NBTTagCompound nbt) {
        if (this.value.length() < 3 && StringUtils.isNumeric(this.value)) {
            nbt.setByte("statueTexture", (byte) EnumStatueTextureType.getFromOrdinal(Integer.parseInt(this.value)).index);
        } else {
            nbt.setByte("statueTexture", (byte) EnumStatueTextureType.getFromString(this.value).index);
        }
    }
}