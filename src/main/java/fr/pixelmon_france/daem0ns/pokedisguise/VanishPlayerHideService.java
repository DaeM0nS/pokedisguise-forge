package fr.pixelmon_france.daem0ns.pokedisguise;

import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.EntityTrackerEntry;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketSpawnPlayer;
import net.minecraft.util.IntHashMap;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class VanishPlayerHideService
        implements PlayerHideService {

    @Override
    public void hide(EntityPlayerMP player) {
        WorldServer world = (WorldServer) player.world;
        try {
            Field f = ObfuscationReflectionHelper.findField(EntityTracker.class, "field_72794_c");
            f.setAccessible(true);
            @SuppressWarnings("unchecked")
            IntHashMap<EntityTrackerEntry> trackedEntityHashTable = (IntHashMap<EntityTrackerEntry>) f.get(world.getEntityTracker());
            EntityTrackerEntry tracker = trackedEntityHashTable.lookup(player.getEntityId());
            if (tracker != null) {
                Set<EntityPlayerMP> tracked = new HashSet<>(tracker.trackingPlayers);
                world.getEntityTracker().untrack(player);
                tracked.forEach(trackedPlayer -> player.connection.sendPacket(new SPacketSpawnPlayer(trackedPlayer)));
                player.setInvisible(true);
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show(EntityPlayerMP player) {
        WorldServer world = (WorldServer) player.world;
        world.getEntityTracker().track(player);
        player.setInvisible(false);
    }
}

