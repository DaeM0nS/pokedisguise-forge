package fr.pixelmon_france.daem0ns.pokedisguise;

import com.google.common.collect.Lists;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.client.models.smd.AnimationType;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.pixelmonmod.pixelmon.enums.EnumBoundingBoxMode;
import com.pixelmonmod.pixelmon.enums.EnumStatueTextureType;
import com.pixelmonmod.pixelmon.util.helpers.ReflectionHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.util.text.event.HoverEvent.Action;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import org.apache.commons.lang3.StringUtils;

import javax.vecmath.Vector3d;
import java.lang.reflect.Field;
import java.util.*;

@Mod(
        modid = "pokedisguise",
        name = "pokedisguise",
        version = "1.0.1-forge",
        acceptableRemoteVersions = "*",
        acceptedMinecraftVersions = "[1.12.2]",
        dependencies = "required-after:pixelmon",
        serverSideOnly = true
)
public class PokeDisguise {

    private static Map<EntityPlayerMP, EntityStatue> disguises = new HashMap<>();

    private static final HashMap<UUID, Vector3d> playerLastPos = new HashMap<>();

    int time = 0;
    boolean started = false;

    public PermissionDisguiseRegistryService pdrs;
    public VanishPlayerHideService vphs;
    public static DisguiseRegistryService service;

    private static PokeDisguise instance;

    private static ITextComponent msg;

    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event) {
        instance = this;
        pdrs = new PermissionDisguiseRegistryService();
        vphs = new VanishPlayerHideService();
        MinecraftForge.EVENT_BUS.register(this);

        PokemonSpec.extraSpecTypes.add(new StatueTextureSpec(Lists.newArrayList("statuetexture"), null));
    }

    @Mod.EventHandler
    public void onPostInit(FMLServerStartingEvent event) {
        service = pdrs;
        event.registerServerCommand(new PokedisguiseCommand());
        event.registerServerCommand(new Callback());
    }

    @SubscribeEvent
    public void onPlayerLeave(PlayerLoggedOutEvent event) {
        if (event.player != null) {
            PokeDisguise.removeDisguise((EntityPlayerMP) event.player);
            service.save((EntityPlayerMP) event.player);
        }
    }

    @SubscribeEvent
    public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
        if (event.player != null) {
            EntityPlayer player = event.player;
            playerLastPos.put(player.getUniqueID(), new Vector3d(player.posX, player.posY, player.posZ));
        }
    }

    @Mod.EventHandler
    public void onServerStopping(FMLServerStoppingEvent event) {
        started = false;
        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            PokeDisguise.removeDisguise(player);
            service.save(player);
        }
    }

    @Mod.EventHandler
    public void onServerStarted(FMLServerStartedEvent event) {
        this.started = true;
    }

    @SubscribeEvent
    public void onTick(TickEvent.ServerTickEvent event) {
        if (!started)
            return;
        if (event.phase == Phase.START) {
            getDisguises().forEach((player, statue) -> {
                if (!((Entity) statue).isEntityAlive()) {
                    NBTTagCompound nbt = statue.getEntityData();
                    boolean hidden = statue.getEntityData().getBoolean("hidden");
                    PokemonSpec spec = new PokemonSpec(nbt.getString("statueSpecs").split(", "));
                    statue = new EntityStatue(player.world);
                    statue.setPokemon(spec.create());
                    spec.apply(statue.getEntityData());
                    String speclist = Arrays.asList(spec.args).toString();
                    statue.getEntityData().setString("statueSpecs", speclist.substring(1, speclist.length() - 1));

                    statue.getEntityData().setBoolean("hidden", hidden);
                    for (String key : nbt.getKeySet()) {
                        if (key.equalsIgnoreCase("hidden")) {
                            statue.getEntityData().setTag(key, nbt.getTag(key));
                        }
                    }

                    if (spec.shiny != null && spec.shiny) {
                        statue.setTextureType(EnumStatueTextureType.Shiny);
                    }

                    if(statue.getEntityData().hasKey("statueTexture")) {
                        String value = String.valueOf(statue.getEntityData().getByte("statueTexture"));
                        if (value.length() < 3 && StringUtils.isNumeric(value)) {
                            statue.setTextureType(EnumStatueTextureType.getFromOrdinal(Integer.parseInt(value)));
                        } else {
                            statue.setTextureType(EnumStatueTextureType.getFromString(value));
                        }
                    }

                    if(Arrays.stream(spec.args).anyMatch(s -> s.startsWith("ct:") || s.startsWith("customtexture:"))){
                        /*statue.customTexture = "";
                        DataParameter<String> dwCustomTexture = EntityDataManager.createKey(EntityStatue.class, DataSerializers.STRING);
                        statue.getDataManager().set(dwCustomTexture, nbt.getString("CustomTexture"));*/
                        DataParameter<String> dwCustomTexture = ReflectionHelper.getPrivateValue(EntityStatue.class, statue, "dwCustomTexture");
                        statue.getDataManager().set(dwCustomTexture, Arrays.stream(spec.args).filter(s -> s.startsWith("ct:") || s.startsWith("customtexture:")).findFirst().get().split(":")[1]);
                        //.getEntityData().setString("CustomTexture", Arrays.stream(spec.args).filter(s -> s.startsWith("ct:") || s.startsWith("customtexture:")).findFirst().get().split(":")[1]);
                    }

                    PokeDisguise.setupDisguise(statue, player);
                    if (hidden) {
                        player.removeEntity(statue);
                    }

                    getDisguises().put(player, statue);
                }
                BlockPos pos = player.getPosition();
                statue.moveToBlockPosAndAngles(pos, player.rotationYawHead, player.rotationPitch);

                //statue.setPosition(pos.getX(), pos.getY(), pos.getZ());
                statue.setRotation(player.rotationYawHead);
                /*Vector3d speed = player.getVelocity().abs();
                boolean isMoving = player.isSprinting() || speed.getX() + speed.getZ() > 0.2;*/

                /*boolean haveSwimAnim = getAllAnimations(statue).stream().anyMatch(a -> a.equals(AnimationType.SWIM));
                boolean haveWalkAnim = getAllAnimations(statue).stream().anyMatch(a -> a.equals(AnimationType.WALK));
                boolean haveFlyAnim = getAllAnimations(statue).stream().anyMatch(a -> a.equals(AnimationType.FLY));*/

                Vector3d oldPos = playerLastPos.get(player.getUniqueID());
                boolean isMoving = player.isSprinting() || player.fallDistance != 0.0F || oldPos.x != player.posX /*|| oldPos.y != player.posY */ || oldPos.z != player.posZ;
                //boolean isMoving = player.isSprinting() || player.getDistance(oldPos.x, oldPos.y, oldPos.z) != 0.0D;
                //boolean isMoving = player.isSprinting() || player.motionX * player.motionZ > 0.2D;

                if (player.isInWater() || player.isInLava()) {
                    if (statue.getIsFlying()) {
                        statue.setIsFlying(false);
                    }
                    //if (haveSwimAnim) {
                    if (!statue.getAnimation().equals(AnimationType.SWIM)) {
                        statue.setAnimation(AnimationType.SWIM);
                    }
                    if (!statue.getAnimation().equals(AnimationType.SWIM)) {
                        statue.setAnimation(AnimationType.IDLE);
                    }
                    //} else {
                        /*if (isMoving) {
                            if (!statue.getAnimation().equals(AnimationType.WALK) && haveWalkAnim) {
                                statue.setAnimation(AnimationType.WALK);
                            }
                        } else {
                            statue.setAnimation(AnimationType.IDLE);
                        }*/
                    //}
                } else if (isMoving) {
                    if (!player.onGround || player.isElytraFlying()) {
                        if (!statue.getAnimation().equals(AnimationType.FLY)/* && haveFlyAnim*/) {
                            if (!statue.getIsFlying()) {
                                statue.setIsFlying(true);
                            }
                            statue.setAnimation(AnimationType.FLY);
                            if (!statue.getAnimation().equals(AnimationType.FLY)) {
                                statue.setAnimation(AnimationType.IDLE);
                            }
                        }/* else {
                            if (!statue.getAnimation().equals(AnimationType.WALK) && haveWalkAnim) {
                                statue.setAnimation(AnimationType.WALK);
                            } else {
                                statue.setAnimation(AnimationType.IDLE);
                            }
                        }*/
                    } else {
                        if (statue.getIsFlying()) {
                            statue.setIsFlying(false);
                        }
                        if (!statue.getAnimation().equals(AnimationType.WALK)/* && haveWalkAnim*/) {
                            statue.setAnimation(AnimationType.WALK);
                        }
                    }
                } else {
                    if (!player.onGround || player.isElytraFlying()) {
                        if (!statue.getAnimation().equals(AnimationType.FLY)/* && haveFlyAnim*/) {
                            if (!statue.getIsFlying()) {
                                statue.setIsFlying(true);
                            }
                            statue.setAnimation(AnimationType.FLY);
                            if (!statue.getAnimation().equals(AnimationType.FLY)) {
                                statue.setAnimation(AnimationType.IDLE);
                            }
                        }/* else {
                            if (!statue.getAnimation().equals(AnimationType.WALK) && haveWalkAnim) {
                                statue.setAnimation(AnimationType.WALK);
                            } else {
                                statue.setAnimation(AnimationType.IDLE);
                            }
                        }*/
                    } else {
                        if (statue.getIsFlying()) {
                            statue.setIsFlying(false);
                        }
                        statue.setAnimation(AnimationType.IDLE);
                    }
                }
                if (isMoving) {
                    playerLastPos.put(player.getUniqueID(), new Vector3d(player.posX, player.posY, player.posZ));
                }
            });

            if (time >= 1200) {
                for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
                    PokeDisguise.service.save(player);
                }
                time = 0;
            } else {
                time += 1;
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void disguise(EntityPlayerMP player, PokemonSpec spec) {
        PokeDisguise.removeDisguise(player);
        EntityStatue statue = new EntityStatue(player.getServerWorld());
        statue.setPokemon(spec.create());
        spec.apply(statue.getEntityData());
        String speclist = Arrays.asList(spec.args).toString();
        statue.getEntityData().setString("statueSpecs", speclist.substring(1, speclist.length() - 1));

        if (spec.shiny != null && spec.shiny) {
            statue.setTextureType(EnumStatueTextureType.Shiny);
        }

        if(statue.getEntityData().hasKey("statueTexture")) {
            String value = String.valueOf(statue.getEntityData().getByte("statueTexture"));
            if (value.length() < 3 && StringUtils.isNumeric(value)) {
                statue.setTextureType(EnumStatueTextureType.getFromOrdinal(Integer.parseInt(value)));
            } else {
                statue.setTextureType(EnumStatueTextureType.getFromString(value));
            }
        }

        if(Arrays.stream(spec.args).anyMatch(s -> s.startsWith("ct:") || s.startsWith("customtexture:"))){
                        /*statue.customTexture = "";
                        DataParameter<String> dwCustomTexture = EntityDataManager.createKey(EntityStatue.class, DataSerializers.STRING);
                        statue.getDataManager().set(dwCustomTexture, nbt.getString("CustomTexture"));*/
            DataParameter<String> dwCustomTexture = ReflectionHelper.getPrivateValue(EntityStatue.class, statue, "dwCustomTexture");
            statue.getDataManager().set(dwCustomTexture, Arrays.stream(spec.args).filter(s -> s.startsWith("ct:") || s.startsWith("customtexture:")).findFirst().get().split(":")[1]);
                    //.getEntityData().setString("CustomTexture", Arrays.stream(spec.args).filter(s -> s.startsWith("ct:") || s.startsWith("customtexture:")).findFirst().get().split(":")[1]);
        }

        PokeDisguise.setupDisguise(statue, player);
        Optional.ofNullable(getDisguises().put(player, statue)).ifPresent(Entity::setDead);

        TextComponentString msg1 = new TextComponentString(TextFormatting.RED + "" + TextFormatting.BOLD + "PokeDisguise " + TextFormatting.GRAY + "" + TextFormatting.BOLD + ">" + TextFormatting.RESET + " Click " + TextFormatting.GREEN + "HERE" + TextFormatting.RESET + " to hide your disguise");
        msg1.getStyle().setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new TextComponentString("Hiding your disguise makes it so others can see it but you can't")));

        TextComponentString msg2 = new TextComponentString(TextFormatting.RED + "" + TextFormatting.BOLD + "PokeDisguise " + TextFormatting.GRAY + "" + TextFormatting.BOLD + ">" + TextFormatting.RESET + " Click " + TextFormatting.GREEN + "HERE" + TextFormatting.RESET + " to unhide your disguise");
        msg2.getStyle().setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new TextComponentString("Unhiding your disguise makes it so you can see it")));

        msg = TextUtils.addCallback(msg1, sender1 -> {
            disguises.get(player).getEntityData().setBoolean("hidden", true);
            player.removeEntity(disguises.get(player));
            ITextComponent msgg = TextUtils.addCallback(msg2, sender2 -> {
                disguises.get(player).getEntityData().setBoolean("hidden", false);
                disguises.get(player).setDead();
                player.sendMessage(msg);
            });
            player.sendMessage(msgg);
        });

        player.sendMessage(msg);
    }

    public static void removeDisguise(EntityPlayerMP player) {
        Optional.ofNullable(getDisguises().remove(player)).ifPresent(entityStatue -> {
            entityStatue.setDead();
            PokeDisguise.getInstance().vphs.show(player);
        });
    }

    private static void setupDisguise(EntityStatue statue, EntityPlayerMP player) {
        statue.setPosition(player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ);
        try {
            Field dwBox = EntityStatue.class.getDeclaredField("dwBoundMode");
            dwBox.setAccessible(true);
            statue.getDataManager().set((DataParameter<Byte>) dwBox.get(null), (byte) EnumBoundingBoxMode.None.ordinal());
        } catch (Exception e) {
            e.printStackTrace();
        }
        statue.setAnimation(AnimationType.IDLE);
        statue.setAnimate(true);
        PokeDisguise.getInstance().vphs.hide(player);
        player.world.spawnEntity(statue);
    }


    public static DisguiseRegistryService getService() {
        return service;
    }

    public void setService(DisguiseRegistryService service) {
        PokeDisguise.service = service;
    }

    public static Map<EntityPlayerMP, EntityStatue> getDisguises() {
        return disguises;
    }

    public static void setDisguises(Map<EntityPlayerMP, EntityStatue> disguises) {
        PokeDisguise.disguises = disguises;
    }

    public static PokeDisguise getInstance() {
        return instance;
    }
}

