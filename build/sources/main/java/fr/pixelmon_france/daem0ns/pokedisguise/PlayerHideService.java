/*
 * Decompiled with CFR 0.146.
 * 
 * Could not load the following classes:
 *  org.spongepowered.api.entity.living.player.Player
 */
package fr.pixelmon_france.daem0ns.pokedisguise;

import net.minecraft.entity.player.EntityPlayerMP;

public interface PlayerHideService {
    public void hide(EntityPlayerMP var1);

    public void show(EntityPlayerMP var1);
}

