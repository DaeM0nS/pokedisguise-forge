/*
 * Decompiled with CFR 0.146.
 * 
 * Could not load the following classes:
 *  com.pixelmonmod.pixelmon.enums.EnumSpecies
 *  org.spongepowered.api.entity.living.player.Player
 */
package fr.pixelmon_france.daem0ns.pokedisguise;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import com.pixelmonmod.pixelmon.enums.EnumSpecies;

import net.minecraft.entity.player.EntityPlayerMP;

public interface DisguiseRegistryService {
    public boolean hasDisguise(EntityPlayerMP var1, String var2);

    public void save(EntityPlayerMP var1);

    public void giveDisguise(EntityPlayerMP var1, String var2);
    
    public void removeDisguise(EntityPlayerMP var1, String var2);

    default public Set<String> getAllDisguises(EntityPlayerMP player) {
        return Arrays.stream(EnumSpecies.values()).map(e -> e.name).filter(n -> this.hasDisguise(player, (String)n)).collect(Collectors.toSet());
    }
}

