/*

 * Decompiled with CFR 0.146.
 * 
 * Could not load the following classes:
 *  org.spongepowered.api.data.DataTransactionResult
 *  org.spongepowered.api.data.key.Key
 *  org.spongepowered.api.data.key.Keys
 *  org.spongepowered.api.entity.living.player.Player
 */
package fr.pixelmon_france.daem0ns.pokedisguise;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.EntityTrackerEntry;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketSpawnPlayer;
import net.minecraft.util.IntHashMap;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class VanishPlayerHideService
implements PlayerHideService {
	
    @Override
    public void hide(EntityPlayerMP player) {
        WorldServer world = (WorldServer) player.world;
    	Field f;
		try {
            f = ObfuscationReflectionHelper.findField(EntityTracker.class,"field_72794_c");
			//Marche f = ReflectionHelper.findField(EntityTracker.class,"field_72794_c","trackedEntityHashTable","2");
            //f = EntityTracker.class.getDeclaredField("trackedEntityHashTable");
        	f.setAccessible(true);
        	@SuppressWarnings("unchecked")
			IntHashMap<EntityTrackerEntry> trackedEntityHashTable = (IntHashMap<EntityTrackerEntry>) f.get(world.getEntityTracker());
            EntityTrackerEntry tracker = trackedEntityHashTable.lookup(player.getEntityId());
            Set<EntityPlayerMP> tracked = new HashSet<>(tracker.trackingPlayers);
            world.getEntityTracker().untrack(player);
            tracked.forEach(tP -> {
                player.connection.sendPacket(new SPacketSpawnPlayer(tP));
            });
            //player.setInvisible(true);
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void show(EntityPlayerMP player) {
        WorldServer world = (WorldServer) player.world;
        world.getEntityTracker().track(player);
    }
}

